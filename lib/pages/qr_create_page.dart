import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QRCreatePage extends StatefulWidget {
  const QRCreatePage({Key? key}) : super(key: key);

  @override
  State<QRCreatePage> createState() => _QRCreatePageState();
}

class _QRCreatePageState extends State<QRCreatePage> {
  final TextEditingController _enterText = TextEditingController();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text(
          'QR Code Generator',
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              QrImage(
                data: _enterText.text,
                version: QrVersions.auto,
                size: MediaQuery.of(context).size.width / 1.5,
                backgroundColor: Colors.white.withOpacity(0.9),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.all(28.0),
                child: _buildTextField(context),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTextField(BuildContext context) {
    return TextField(
      controller: _enterText,
      decoration: InputDecoration(
        hintText: 'Enter data',
        hintStyle: TextStyle(color: Colors.grey),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: Colors.white),
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: Colors.cyanAccent)),
        suffixIcon: IconButton(
          onPressed: () {
            setState(() {
              // FocusScope.of(context).requestFocus(FocusNode());
              FocusScope.of(context).unfocus();
            });
          },
          color: Colors.amber,
          icon: Icon(
            Icons.done_outline,
            size: 30,
          ),
        ),
      ),
    );
  }
}
