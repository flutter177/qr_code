import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRScanPage extends StatefulWidget {
  const QRScanPage({Key? key}) : super(key: key);

  @override
  State<QRScanPage> createState() => _QRScanPageState();
}

class _QRScanPageState extends State<QRScanPage> {
  Barcode? result;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void reassemble() async {
    // TODO: implement reassemble
    super.reassemble();
    if (Platform.isAndroid) {
      await controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: Text('QR Scan Code'),
        ),
        body: SafeArea(
          child: Stack(
            alignment: Alignment.center,
            children: [
              _buildQrView(context),
              Positioned(bottom: 30, child: buildResult()),
              Positioned(top: 20, child: buildControlButtons()),
            ],
          ),
        ));
  }

  Widget _buildQrView(BuildContext context) {
    return QRView(
      key: qrKey,
      onQRViewCreated: onQRViewCreated,
      overlay: QrScannerOverlayShape(
        borderWidth: 10,
        borderLength: 20,
        borderRadius: 10,
        cutOutSize: MediaQuery.of(context).size.width * 0.8,
      ),
    );
  }

  void onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });

    controller.scannedDataStream.listen((barcode) {
      controller.pauseCamera();
      setState(() {
        this.result = barcode;
      });
    });
  }

  Widget buildResult() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.2,
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white12,
      ),
      child: SelectableText(
        result != null ? 'Result: ${result!.code}' : 'Scan a code!',
        style: TextStyle(fontSize: 18, color: Colors.white),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildControlButtons() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white24,
      ),
      child:
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   crossAxisAlignment: CrossAxisAlignment.center,
          //   children: <Widget>[
          //     Container(
          //       margin: const EdgeInsets.all(8),
          //       child: ElevatedButton(
          //           onPressed: () async {
          //             await controller?.toggleFlash();
          //             setState(() {});
          //           },
          //           child: FutureBuilder(
          //             future: controller?.getFlashStatus(),
          //             builder: (context, snapshot) {
          //               return Text('Flash: ${snapshot.data}');
          //             },
          //           )),
          //     ),
          //     Container(
          //       margin: const EdgeInsets.all(8),
          //       child: ElevatedButton(
          //           onPressed: () async {
          //             await controller?.flipCamera();
          //             setState(() {});
          //           },
          //           child: FutureBuilder(
          //             future: controller?.getCameraInfo(),
          //             builder: (context, snapshot) {
          //               controller!.resumeCamera();
          //               if (snapshot.data != null) {
          //                 return Text(
          //                     'Camera facing ${describeEnum(snapshot.data!)}');
          //               } else {
          //                 return const Text('loading');
          //               }
          //             },
          //           )),
          //     )
          //   ],
          // ),
          Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            onPressed: () async {
              await controller?.toggleFlash();
              setState(() {});
            },
            icon: FutureBuilder(
              future: controller?.getFlashStatus(),
              builder: (context, snapshot) {
                if (snapshot.data != null) {
                  controller!.resumeCamera();
                  return (snapshot.data == true)
                      ? Icon(Icons.flash_on)
                      : Icon(Icons.flash_off);
                } else
                  return Container();
              },
            ),
          ),
          IconButton(
            onPressed: () async {
              await controller?.flipCamera();
              setState(() {});
            },
            icon: FutureBuilder(
              future: controller?.getCameraInfo(),
              builder: (context, snapshot) {
                print('camera = ${snapshot.data}');
                if (snapshot.data != null) {
                  return Icon(Icons.switch_camera);
                } else
                  return Container();
              },
            ),
          ),
        ],
      ),
    );
  }
}
